#!/bin/bash

Help()
{
   # Display Help
   echo "nrkfzf is a script that search on tv.nrk.no and pipes it to fzf"
   echo
   echo "Syntax: nrkfzf [-c|h|t|v|V]"
   echo "options:"
   echo "c     Search with category"
   echo "h     Print this Help."
   echo "v     Print software version."
   echo "V     Print software version and exit."
   echo
}

VERSION()
{
echo "nrkfzf: 1.0.0"
mpv --version | head -n 1
}

while getopts ":h :v" option; do
   case $option in
     h) # display Help
         Help
         exit;;
     v) # display Help
         VERSION 
	 exit;;
     \?) # incorrect option
         echo "Error: Invalid option"
         exit;;
   esac
done

#read sok echo "test"
echo -n "Search on nrk.tv: ";
read sok;

#en=echo "https://tv.nrk.no/"

curl -s "https://psapi.nrk.no/search?q=$sok&includeHighLights=false&page=0&maxResultsPerPage=20" |  jq -r '.hits'"[]".'hit'.'url' | awk '{ print "https://tv.nrk.no/" $0 }' |mpv "$(fzf)"

